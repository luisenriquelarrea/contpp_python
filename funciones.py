import numpy
import json
import inspect
from datetime import datetime, date, timedelta
from typing import List

class Funciones:
    def set_time(self, fecha: str):
        fecha = datetime.strptime(fecha, '%Y-%m-%d %H:%M:%S')
        fecha = fecha - timedelta(hours=1)
        return str(fecha);
    
    def array_column(self, column: int, valores):
        return [(str(valor[column])) for valor in valores]

    def array_str_replace(self, search_string, replace_string, valores):
        return numpy.where(valores == search_string, replace_string, valores)

    def array_unique(self, valores):
        return list(set([item[0] for item in valores]))

    def date_range(self, start_date, end_date):
        delta = timedelta(days=1)
        items = []
        while start_date <= end_date:
            items.append(start_date)
            start_date += delta
        return items
    
    def day_spanish(self, day: str):
        days = {
            'sunday': 'domingo',
            'monday': 'lunes',
            'tuesday': 'martes',
            'wednesday': 'miercoles',
            'thursday': 'jueves',
            'friday': 'viernes',
            'saturday': 'sabado',
        }
        if days.get(day) is None:
            return {
                'error': True, 
                'mensaje': 'Error day '+day+' no existe en la config', 
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        return {
            'data': days.get(day)
        }
    
    def dict_column(self, column: str, valores):
        return [valor.get(column) for valor in valores]

    def in_dictionary(self, needle, haystack):
        keys = haystack.keys()
        for key in needle:
            if key not in keys:
                return {
                    'error': True, 
                    'mensaje': 'Error key '+key+' debe existir', 
                    'file': __file__, 
                    'line': inspect.currentframe().f_lineno
                }
        return {
            "mensaje":"Exito"
        }
    
    def today(self):
        return date.today()

    def parse_string_to_datetime(self, fecha: str):
        fecha = fecha + " 00:00:00"
        fecha = datetime.strptime(fecha, '%Y-%m-%d %H:%M:%S')
        return fecha

    def parse_json_data(self, response):
        json_data = json.loads(response)
        return json_data

    def pretty_json(self, json_data):
        json_data = json.dumps(json_data, indent=4, sort_keys=False)
        return json_data

    def str_cut_string(self, cadena, n_positions):
        len_cadena = len(cadena)
        cadena = cadena[:len_cadena - n_positions]
        return cadena

    def str_implode(self, caracter, valores):
        return caracter.join(valores)

    def str_replace(self, characters, replacement, input):
        for char in characters:
            input = input.replace(char, replacement)
        return input

    def time_diff(self, start_time:str, end_time:str):
        t1 = datetime.strptime(start_time, "%H:%M:%S")
        t2 = datetime.strptime(end_time, "%H:%M:%S")
        delta = t2 - t1
        total_minutes = delta.total_seconds() / 60
        return total_minutes

    def to_dictionary(self, encabezados, valores):
        return dict(zip(encabezados, valores))

    def to_str_array_values(self, valores):
        return [str(i) for i in valores]