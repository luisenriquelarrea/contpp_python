SQL_OBTEN_APROXIMACION_NETO = "SELECT "+\
    "sd, "+\
    "sdi, "+\
    "sueldos_salarios, "+\
    "subsidio_empleo_entregado, "+\
    "isr_pagar, "+\
    "imss, "+\
    "neto "+\
    "FROM sueldos_salarios "+\
    "ORDER BY ABS(neto - {neto}) "+\
    "LIMIT 2"

SQL_LIMPIA_SUELDOS_SALARIOS = "DELETE FROM sueldos_salarios"

SQL_OBTEN_DIAS_PAGADOS_CURRENT_BASE = "SELECT DISTINCT n_dias_pagados "+\
    "FROM sueldos_salarios "+\
    "LIMIT 1"

SQL_OBTEN_DIAS_VACACIONES_CURRENT_BASE = "SELECT DISTINCT n_dias_vacaciones "+\
    "FROM sueldos_salarios "+\
    "LIMIT 1"

SQL_OBTEN_EJERCICIO = "SELECT id " +\
    "FROM ejercicio " +\
    "WHERE descripcion = '{ejercicio}'"

SQL_OBTEN_EJERCICIO_CURRENT_BASE = "SELECT DISTINCT ejercicio.descripcion "+\
    "FROM sueldos_salarios "+\
    "INNER JOIN ejercicio ON ejercicio.id = sueldos_salarios.ejercicio_id "+\
    "LIMIT 1"

SQL_OBTEN_ISR = "SELECT id, limite_inferior, limite_superior, cuota_fija, porcentaje_excedente "+\
    "FROM {isr} "+\
    "WHERE {monto_gravado} >= limite_inferior "+\
    "AND {monto_gravado} <= limite_superior "+\
    "AND '{fecha_pago}' >= fecha_inicio "+\
    "AND '{fecha_pago}' <= fecha_final "

SQL_OBTEN_SALARIO_DIARIO_INICIO_CURRENT_BASE = "SELECT sd "+\
    "FROM sueldos_salarios "+\
    "ORDER BY sd ASC "+\
    "LIMIT 1"

SQL_OBTEN_SALARIO_DIARIO_FINAL_CURRENT_BASE = "SELECT sd "+\
    "FROM sueldos_salarios "+\
    "ORDER BY sd DESC "+\
    "LIMIT 1"

SQL_OBTEN_SALARIO_MINIMO = "SELECT id, centro, frontera "+\
    "FROM salario_minimo "+\
    "WHERE '{fecha}' >= fecha_inicio "+\
    "AND '{fecha}' <= fecha_final"

SQL_OBTEN_SUBSIDIO_EMPLEO = "SELECT id, cuota_fija "+\
    "FROM {subsidio_empleo} "+\
    "WHERE {monto_gravado} >= limite_inferior "+\
    "AND {monto_gravado} <= limite_superior "+\
    "AND '{fecha_pago}' >= fecha_inicio "+\
    "AND '{fecha_pago}' <= fecha_final "

SQL_OBTEN_SUELDOS_SALARIOS_CALCULAR = "SELECT sueldos_salarios.id, sueldos_salarios.n_dias_pagados, " +\
    "sueldos_salarios.n_dias_vacaciones, sueldos_salarios.sd, "+\
    "sueldos_salarios.bono_septimo_dia, sueldos_salarios.bono_puntualidad, sueldos_salarios.bono_asistencia, "+\
    "ejercicio.descripcion "+\
    "FROM sueldos_salarios "+\
    "INNER JOIN ejercicio ON ejercicio.id = sueldos_salarios.ejercicio_id "+\
    "WHERE sueldos_salarios.sueldos_salarios = 0.0000 "

SQL_OBTEN_UMA = "SELECT id, monto "+\
    "FROM uma "+\
    "WHERE '{fecha}' >= fecha_inicio "+\
    "AND '{fecha}' <= fecha_final"