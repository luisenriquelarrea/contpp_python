import MySQLdb
import inspect
import numpy
import config
import queries
from funciones import Funciones

class Nomina:
    connection_mysql = False
    cursor_mysql = False

    def __init__(self):
        mysql = config.MYSQL_LOCALHOST
        self.connection_mysql = MySQLdb.Connect(
            user=mysql['username'], 
            passwd=mysql['password'], 
            db=mysql['database'],
            host=mysql['host']
        )
        self.cursor_mysql = self.connection_mysql.cursor()

    def asigna_calculos_sueldos_salarios(self, valores):
        for registro in valores:
            n_dias_pagados = int(registro[1])
            bono_septimo_dia = registro[4]
            ejercicio = registro[7]
            fecha_pago = str(ejercicio) + "-02-01"
            n_dias_reducir = 0
            if bono_septimo_dia > 0.00:
                n_dias_reducir += 1
                if n_dias_pagados == 14 or n_dias_pagados == 15:
                    n_dias_reducir += 1
            salario_diario = float(registro[3])
            sueldos_salarios = self.calcula_sueldos_salarios(salario_diario, n_dias_pagados - n_dias_reducir)
            bono_puntualidad = registro[5]
            bono_asistencia = registro[6]
            total_gravado = sueldos_salarios + bono_septimo_dia + bono_puntualidad + bono_asistencia
            resultado = self.calcula_isr(total_gravado, n_dias_pagados, fecha_pago)
            if resultado.get('error') is not None:
                return {
                    'error': True, 
                    'mensaje': 'Error al calcular ISR', 
                    'data': resultado,
                    'file': __file__, 
                    'line': inspect.currentframe().f_lineno
                }
            isr_determinado = resultado.get('isr_determinado')
            n_dias_vacaciones = int(registro[2])
            resultado = self.calcula_imss(salario_diario, n_dias_pagados, 15, n_dias_vacaciones, fecha_pago)
            if resultado.get('error') is not None:
                return {
                    'error': True, 
                    'mensaje': 'Error al calcular IMSS', 
                    'data': resultado,
                    'file': __file__, 
                    'line': inspect.currentframe().f_lineno
                }
            imss = resultado.get('imss')
            resultado = self.calcula_subsidio_empleo(total_gravado, n_dias_pagados, fecha_pago, isr_determinado)
            if resultado.get('error') is not None:
                return {
                    'error': True, 
                    'mensaje': 'Error al calcular subsidio al empleo', 
                    'data': resultado,
                    'file': __file__, 
                    'line': inspect.currentframe().f_lineno
            }
            subsidio_empleo_causado = resultado.get('subsidio_empleo_causado')
            subsidio_empleo_entregado = resultado.get('subsidio_empleo_entregado')
            resultado = self.calcula_isr_pagar(isr_determinado, subsidio_empleo_causado,
                subsidio_empleo_entregado)
            if resultado.get('error') is not None:
                return {
                    'error': True, 
                    'mensaje': 'Error al calcular ISR a pagar', 
                    'data': resultado,
                    'file': __file__, 
                    'line': inspect.currentframe().f_lineno
                }
            isr_pagar = resultado.get('isr_pagar')
            neto = round(total_gravado + subsidio_empleo_entregado - isr_pagar - imss, 2)
            sueldos_salarios_id = registro[0]
            resultado = self.asigna_valores_sueldos_salarios(sueldos_salarios_id, sueldos_salarios,
                subsidio_empleo_causado, subsidio_empleo_entregado, isr_determinado, isr_pagar, imss, neto)
            if resultado.get('error') is not None:
                return {
                    'error': True, 
                    'mensaje': 'Error al asignar valores sueldos y salarios', 
                    'data': resultado,
                    'file': __file__, 
                    'line': inspect.currentframe().f_lineno
                }
        return {
            'mensaje': 'exito'
        }

    def asigna_valores_iniciales(self, valores):
        ejercicio = valores.get('ejercicio')
        sd_inicio = valores.get('sd_inicio')
        sd_final = valores.get('sd_final')
        n_dias_pagados = valores.get('n_dias_pagados')
        n_dias_vacaciones = valores.get('n_dias_vacaciones')
        estrategia_bonos = valores.get('estrategia_bonos')
        step = valores.get('step')
        for i in numpy.arange(sd_inicio, sd_final, step):
            salario_diario = i
            resultado = self.calcula_sdi(salario_diario, 15, n_dias_vacaciones, 0.25)
            if resultado.get('error') is not None:
                return {
                    'error': True, 
                    'mensaje': 'Error al calcular SDI', 
                    'data': resultado, 
                    'file': __file__,
                    'line': inspect.currentframe().f_lineno
                }
            sdi = resultado.get('sdi')
            bono_septimo_dia = 0
            bono_puntualidad = 0
            bono_asistencia = 0
            if int(estrategia_bonos) == 1:
                resultado = self.calcula_bonos_estrategia(i, n_dias_pagados)
                bono_septimo_dia = resultado.get('bono_septimo_dia')
                bono_puntualidad = resultado.get('bono_puntualidad')
                bono_asistencia = resultado.get('bono_asistencia')
            sql = "INSERT INTO sueldos_salarios(ejercicio, n_dias_pagados, n_dias_vacaciones, sd, sdi, "+\
                "bono_septimo_dia, bono_puntualidad, bono_asistencia) "+\
                "VALUES(%s, %s, %s, %s, %s, %s, %s, %s)"
            values = [ejercicio, n_dias_pagados, n_dias_vacaciones, salario_diario, sdi, bono_septimo_dia, 
                bono_puntualidad, bono_asistencia]
            resultado = self.ejecuta_sql(sql, values)
            if resultado.get('error') is not None:
                return {
                    'error': True, 
                    'mensaje': 'Error al ejecutar SQL', 
                    'data': resultado, 
                    'file': __file__,
                    'line': inspect.currentframe().f_lineno
                }
        return {
            'mensaje': 'Exito'
        }

    def asigna_valores_sueldos_salarios(self, sueldos_salarios_id, sueldos_salarios, 
        subsidio_empleo_causado, subsidio_empleo_entregado, isr_determinado,
        isr_pagar, imss, neto):
        sql = "UPDATE sueldos_salarios "+\
            "SET sueldos_salarios = %s, "+\
            "subsidio_empleo_causado = %s, "+\
            "subsidio_empleo_entregado = %s, "+\
            "isr_determinado = %s, "+\
            "isr_pagar = %s, "+\
            "imss = %s, "+\
            "neto = %s "+\
            "WHERE id = %s"
        valores = [sueldos_salarios, subsidio_empleo_causado, subsidio_empleo_entregado, 
            isr_determinado, isr_pagar, imss, neto, sueldos_salarios_id]
        resultado = self.ejecuta_sql(sql, valores)
        if resultado.get('error') is not None:
            return {
                'error': True, 
                'mensaje': 'Error al ejecutar SQL', 
                'data': resultado, 
                'file': __file__,
                'line': inspect.currentframe().f_lineno
            }
        return {
            'mensaje': 'exito'
        }

    def calcula_bonos_estrategia(self, salario_diario, n_dias_pagados):
        n_septimo_dias = 1
        if n_dias_pagados == 14 or n_dias_pagados == 15:
            n_septimo_dias = 2
        bono_septimo_dia = salario_diario * n_septimo_dias
        bono_puntualidad = salario_diario * n_dias_pagados * 0.1
        bono_asistencia = bono_puntualidad
        return {
            'bono_septimo_dia': bono_septimo_dia,
            'bono_puntualidad': bono_puntualidad,
            'bono_asistencia': bono_asistencia
        }

    def calcula_imss(self, salario_diario, n_dias_pagados, n_dias_aguinaldo, n_dias_vacaciones, fecha_pago):
        resultado = self.calcula_salario_base_cotizacion(salario_diario, n_dias_aguinaldo, n_dias_vacaciones)
        if resultado.get('error') is not None:
            return {
                'error': True, 
                'mensaje': 'Error al calcular SBC', 
                'data': resultado,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        SBC = resultado.get('sbc')
        resultado = self.obten_uma(fecha_pago)
        if resultado.get('error') is not None:
            return {
                'error': True, 
                'mensaje': 'Error al obtener UMA', 
                'data': resultado,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        UMA = resultado.get('uma')
        IMSS = 0
        diferencia_3uma_sbc = SBC - (UMA * 3)
        if diferencia_3uma_sbc > 0:
            IMSS = (0.40 / 100) * diferencia_3uma_sbc * n_dias_pagados

        #2.2 Gastos médicos para pensionados y beneficiarios
        IMSS = IMSS + (0.375 / 100) * SBC * n_dias_pagados

        #2.3 En dinero
        IMSS = IMSS + (0.25 / 100) * SBC * n_dias_pagados

        #3 Invalidez y Vida

        #3.1 En especie y dinero
        IMSS = IMSS + (0.625 / 100) * SBC * n_dias_pagados

        #4 Retiro, Cesantía en Edad Avanzada y Vejez (CEAV)
        IMSS = IMSS + (1.125 / 100) * SBC * n_dias_pagados

        IMSS = round(IMSS, 2)
        return {
            'imss': IMSS
        }

    def calcula_isr(self, monto_gravado, n_dias_pagados, fecha_pago):
        tabla_isr = False
        if n_dias_pagados == 7:
            tabla_isr = 'isr_semanal'
        if n_dias_pagados == 14:
            tabla_isr = 'isr_catorcenal'
        if n_dias_pagados == 15:
            tabla_isr = 'isr_quincenal'
        if n_dias_pagados == 30:
            tabla_isr = 'isr_mensual'
        if not tabla_isr:
            return {
                'error': True, 
                'mensaje': 'Error los dias pagados no corresponden a los validos (7, 14, 15, 30)',
                'data': tabla_isr, 
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        resultado = self.calcula_isr_periodo(monto_gravado, fecha_pago, tabla_isr)
        if resultado.get('error') is not None:
            return {
                'error': True, 
                'mensaje': 'Error al calcular ISR', 
                'data': resultado,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        isr_determinado = resultado.get('isr_determinado')
        return {'isr_determinado': isr_determinado}

    def calcula_isr_pagar(self, isr_determinado, subsidio_empleo_causado, subsidio_empleo_entregado):
        isr_pagar = isr_determinado - (subsidio_empleo_causado - subsidio_empleo_entregado)
        return {
            'isr_pagar':isr_pagar
        }

    def calcula_isr_periodo(self, monto_gravado, fecha_pago, tabla_isr):
        resultado = self.obten_registro_isr(monto_gravado, fecha_pago, tabla_isr)
        if resultado.get('error') is not None:
            return {
                'error': True, 
                'mensaje': 'Error al obtener registro ISR', 
                'data': resultado,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        registro_isr = resultado.get('registro_isr')
        resultado = self.calculo_isr(registro_isr, monto_gravado)
        if resultado.get('error') is not None:
            return {
                'error': True, 
                'mensaje': 'Error al obtener ISR calculado', 
                'data': resultado,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        isr_determinado = resultado.get('isr_determinado')
        return {'isr_determinado': isr_determinado}

    def calcula_salario_base_cotizacion(self, salario_diario, n_dias_aguinaldo, n_dias_vacaciones):
        dias_anio = 365
        prima_vacacional = 0.25
        SBC = (
            ((n_dias_aguinaldo * salario_diario) / dias_anio ) 
            + ((n_dias_vacaciones * salario_diario * prima_vacacional) / dias_anio ) 
            + salario_diario
        )
        SBC = round(SBC, 4)
        return {
            'sbc': SBC
        }

    def calcula_sdi(self, sd, dias_aguinaldo, dias_vacaciones, prima_vacacional):
        monto_aguinaldo = round(sd * dias_aguinaldo, 2)
        monto_vacaciones = round(sd * dias_vacaciones, 2)
        monto_prima_vacacional = round(prima_vacacional * monto_vacaciones, 2)
        montos_extra = round(monto_aguinaldo + monto_prima_vacacional, 2)
        monto_anual = round((sd * 365) + montos_extra, 2)
        sdi = round(monto_anual / 365, 2)
        return {
            'sdi': sdi
        }

    def calcula_subsidio_empleo(self, monto_gravado, n_dias_pagados, fecha_pago, isr_determinado):
        tabla_subsidio = False
        if n_dias_pagados == 7:
            tabla_subsidio = 'subsidio_empleo_semanal'
        if n_dias_pagados == 14:
            tabla_subsidio = 'subsidio_empleo_catorcenal'
        if n_dias_pagados == 15:
            tabla_subsidio = 'subsidio_empleo_quincenal'
        if n_dias_pagados == 30:
            tabla_subsidio = 'subsidio_empleo_mensual'
        if not tabla_subsidio:
            return {
                'error': True, 
                'mensaje': 'Error los dias pagados no corresponden a los validos (7, 14, 15, 30)',
                'data': tabla_subsidio, 
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        resultado = self.calcula_subsidio_empleo_periodo(monto_gravado, fecha_pago, tabla_subsidio)
        if resultado.get('error') is not None:
            return {
                'error': True, 
                'mensaje': 'Error al calcular subsidio_empleo', 
                'data': resultado,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        subsidio_empleo_causado = resultado.get('subsidio_empleo_causado')
        resultado = self.calcula_subsidio_empleo_entregado(subsidio_empleo_causado, isr_determinado)
        if resultado.get('error') is not None:
            return {
                'error': True, 
                'mensaje': 'Error al calcular subsidio empleo entregado', 
                'data': resultado,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        subsidio_empleo_entregado = resultado.get('subsidio_empleo_entregado')
        return {
            'subsidio_empleo_causado':subsidio_empleo_causado, 
            'subsidio_empleo_entregado': subsidio_empleo_entregado
        }

    def calcula_subsidio_empleo_entregado(self, subsidio_empleo_causado, isr_determinado):
        if isr_determinado >= subsidio_empleo_causado:
            return {
                'subsidio_empleo_entregado': 0.0000
            }
        subsidio_empleo_entregado = round(subsidio_empleo_causado - isr_determinado, 2)
        return {
            'subsidio_empleo_entregado': subsidio_empleo_entregado
        }

    def calcula_subsidio_empleo_periodo(self, monto_gravado, fecha_pago, tabla_subsidio_empleo):
        resultado = self.obten_registro_subsidio_empleo(monto_gravado, fecha_pago, tabla_subsidio_empleo)
        if resultado.get('error') is not None:
            return {
                'error': True, 
                'mensaje': 'Error al obtener registro subsidio empleo', 
                'data': resultado,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        registro_subsidio_empleo = resultado.get('registro_subsidio_empleo')
        subsidio_empleo_causado = registro_subsidio_empleo[1]
        return {
            'subsidio_empleo_causado': subsidio_empleo_causado
        }

    def calcula_sueldos_salarios(self, salario_diario, n_dias_pagados):
        return round(salario_diario * n_dias_pagados, 2)

    def calcula_valores_sueldos_salarios(self):
        resultado = self.obten_sueldos_salarios_calcular()
        if resultado.get('error') is not None:
            return {
                'error': True, 
                'mensaje': 'Error al obtener sueldos salarios', 
                'data': resultado,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        sueldos_salarios = resultado.get('sueldos_salarios')
        resultado = self.asigna_calculos_sueldos_salarios(sueldos_salarios)
        if resultado.get('error') is not None:
            return {
                'error': True, 
                'mensaje': 'Error al asignar calculos sueldos salarios', 
                'data': resultado, 
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        return {
            'mensaje':'exito'
        }

    def calculo_isr(self, registro_isr, monto_gravado):
        limite_inferior = registro_isr[1]
        cuota_fija = registro_isr[3]
        excedente = monto_gravado - limite_inferior
        porcentaje_excedente = registro_isr[4]
        isr_excedente = round(excedente * porcentaje_excedente / 100, 2)
        isr_determinado = round(isr_excedente + cuota_fija, 2)
        return {
            'isr_determinado': isr_determinado
        }

    def ejecuta_sql(self, sql, valores):
        try:
            self.cursor_mysql.execute(sql, (valores))
        except (MySQLdb.Error, MySQLdb.Warning) as e:
            return {
                'error': True, 
                'mensaje': 'Error al ejecutar SQL', 
                'data': e, 
                'sql': sql, 
                'file': __file__,
                'line': inspect.currentframe().f_lineno
            }
        self.connection_mysql.commit()
        registro_id = self.cursor_mysql.lastrowid
        return {
            'registro_id': registro_id
        }

    def limpia_base_sueldos_salarios(self):
        sql = queries.SQL_LIMPIA_SUELDOS_SALARIOS
        valores = []
        resultado = self.ejecuta_sql(sql, valores)
        if resultado.get('error') is not None:
            return {
                'error': True, 
                'mensaje': 'Error al ejecutar SQL', 
                'data': resultado, 
                'file': __file__,
                'line': inspect.currentframe().f_lineno
            }
        return {
            'mensaje': 'exito'
        }

    def obten_aproximacion_neto(self, valores):
        neto = valores.get('neto')
        sql = queries.SQL_OBTEN_APROXIMACION_NETO
        sql = sql.replace('{neto}', str(neto))
        try:
            self.cursor_mysql.execute(sql)
        except (MySQLdb.Error, MySQLdb.Warning) as e:
            return {
                'error': True, 
                'mensaje': 'Error al ejecutar SQL', 
                'data': e, 
                'sql': sql,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        sueldos_salarios = self.cursor_mysql.fetchall()
        num_registros = len(sueldos_salarios)
        if num_registros == 0:
            return {
                'error': True, 
                'mensaje': 'Error no existe informacion sueldos_salarios', 
                'data': sueldos_salarios, 
                'file': __file__,
                'line': inspect.currentframe().f_lineno
            }
        data = {}
        index = 0
        for obj in sueldos_salarios:
            arr = {
                'sd': obj[0],
                'sdi': obj[1],
                'sueldos_salarios': obj[2],
                'subsidio_empleo': obj[3],
                'ISR': obj[4],
                'IMSS': obj[5],
                'neto': obj[6]
            }
            data[index] = arr
            index += 1
        return data

    def obten_registro_isr(self, monto_gravado, fecha_pago, tabla_isr):
        sql = queries.SQL_OBTEN_ISR
        sql = sql.replace('{isr}', str(tabla_isr))
        sql = sql.replace('{monto_gravado}', str(monto_gravado))
        sql = sql.replace('{fecha_pago}', str(fecha_pago))
        try:
            self.cursor_mysql.execute(sql)
        except (MySQLdb.Error, MySQLdb.Warning) as e:
            return {
                'error': True, 
                'mensaje': 'Error al ejecutar SQL', 
                'data': e, 
                'sql': sql, 
                'file': __file__,
                'line': inspect.currentframe().f_lineno
            }
        registro_isr = self.cursor_mysql.fetchone()
        if registro_isr is None or len(registro_isr) == 0:
            return {
                'error': True, 
                'mensaje': 'Error no existe registro en la tabla ISR '+ tabla_isr,
                'data': registro_isr, 
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        return {
            'registro_isr': registro_isr
        }

    def obten_registro_subsidio_empleo(self, monto_gravado, fecha_pago, tabla_subsidio_empleo):
        sql = queries.SQL_OBTEN_SUBSIDIO_EMPLEO
        sql = sql.replace('{subsidio_empleo}', str(tabla_subsidio_empleo))
        sql = sql.replace('{monto_gravado}', str(monto_gravado))
        sql = sql.replace('{fecha_pago}', str(fecha_pago))
        try:
            self.cursor_mysql.execute(sql)
        except (MySQLdb.Error, MySQLdb.Warning) as e:
            return {
                'error': True, 
                'mensaje': 'Error al ejecutar SQL', 
                'data': e, 
                'sql': sql, 
                'file': __file__,
                'line': inspect.currentframe().f_lineno
            }
        registro_subsidio_empleo = self.cursor_mysql.fetchone()
        if registro_subsidio_empleo is None or len(registro_subsidio_empleo) == 0:
            return {
                'error': True, 
                'mensaje': 'Error no existe registro en la tabla subsidio empleo '+tabla_subsidio_empleo,
                'data': registro_subsidio_empleo, 
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        return {
            'registro_subsidio_empleo': registro_subsidio_empleo
        }

    def obten_salario_minimo_inicial(self, fecha):
        sql = queries.SQL_OBTEN_SALARIO_MINIMO
        sql = sql.replace('{fecha}', str(fecha))
        try:
            self.cursor_mysql.execute(sql)
        except (MySQLdb.Error, MySQLdb.Warning) as e:
            return {
                'error': True, 
                'mensaje': 'Error al ejecutar SQL', 
                'data': e, 
                'sql': sql,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        salario_minimo = self.cursor_mysql.fetchone()
        if salario_minimo is None or len(salario_minimo) == 0:
            return {
                'error': True, 
                'mensaje': 'Error no existe salario_minimo', 
                'data': salario_minimo,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        salario_minimo_monto = salario_minimo[1]
        return {
            'salario_minimo_monto': salario_minimo_monto
        }

    def obten_sueldos_salarios_calcular(self):
        sql = queries.SQL_OBTEN_SUELDOS_SALARIOS_CALCULAR
        try:
            self.cursor_mysql.execute(sql)
        except (MySQLdb.Error, MySQLdb.Warning) as e:
            return {
                'error': True, 
                'mensaje': 'Error al ejecutar SQL', 
                'data': e, 
                'sql': sql, 
                'file': __file__,
                'line': inspect.currentframe().f_lineno
            }
        sueldos_salarios = self.cursor_mysql.fetchall()
        num_registros = len(sueldos_salarios)
        if num_registros == 0:
            return {
                'error': True, 
                'mensaje': 'Error no hay mas registros a calcular', 
                'data': sueldos_salarios,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        return {
            'sueldos_salarios': sueldos_salarios
        }

    def obten_uma(self, fecha_pago):
        sql = queries.SQL_OBTEN_UMA
        sql = sql.replace('{fecha}', str(fecha_pago))
        try:
            self.cursor_mysql.execute(sql)
        except (MySQLdb.Error, MySQLdb.Warning) as e:
            return {
                'error': True, 
                'mensaje': 'Error al ejecutar SQL', 
                'data': e, 
                'sql': sql,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        uma = self.cursor_mysql.fetchone()
        if uma is None or len(uma) == 0:
            return {
                'error': True, 
                'mensaje': 'Error no existe UMA', 
                'data': uma,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        uma_monto = uma[1]
        return {
            'uma': uma_monto
        }

    def valida_inputs(self, inputs):
        funciones = Funciones()
        required_keys = [
            "ejercicio", 
            "sd_inicio", 
            "sd_final", 
            "n_dias_pagados", 
            "n_dias_vacaciones", 
            "estrategia_bonos"
        ]
        resultado = funciones.in_dictionary(required_keys, inputs)
        if resultado.get('error') is not None:
            return {
                'error': True, 
                'mensaje': 'Error validar keys in dictionary', 
                'data': resultado,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        ejercicio = inputs.get('ejercicio')
        fecha_inicio_ejercicio = str(ejercicio) + "-02-01"
        resultado = self.obten_salario_minimo_inicial(fecha_inicio_ejercicio)
        if resultado.get('error') is not None:
            return {
                'error': True, 
                'mensaje': 'Error al obtener salario minimo', 
                'data': resultado,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        salario_minimo = resultado.get('salario_minimo_monto')
        sd_inicio = inputs.get('sd_inicio')
        try:
            sd_inicio = float(sd_inicio)
        except ValueError:
            return {
                'error': True, 
                'mensaje': 'Error sd_inicio debe ser numerico', 
                'data': inputs,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        if sd_inicio == 0:
            sd_inicio = salario_minimo
        sd_final = inputs.get('sd_final')
        try:
            sd_final = float(sd_final)
        except ValueError:
            return {
                'error': True, 
                'mensaje': 'Error sd_final debe ser numerico', 
                'data': inputs,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        n_dias_pagados = inputs.get('n_dias_pagados')
        try:
            n_dias_pagados = int(n_dias_pagados)
        except ValueError:
            return {
                'error': True, 
                'mensaje': 'Error n_dias_pagados debe ser numerico', 
                'data': inputs,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        n_dias_vacaciones = inputs.get('n_dias_vacaciones')
        try:
            n_dias_vacaciones = int(n_dias_vacaciones)
        except ValueError:
            return {
                'error': True, 
                'mensaje': 'Error n_dias_vacaciones debe ser numerico', 
                'data': inputs,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        estrategia_bonos = inputs.get('estrategia_bonos')
        try:
            estrategia_bonos = int(estrategia_bonos)
        except ValueError:
            return {
                'error': True, 
                'mensaje': 'Error estrategia_bonos debe ser numerico', 
                'data': inputs,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        if inputs.get('step') is None:
            input['step'] = 0.25
        return {
            "mensaje": "Exito",
            "data": inputs
        }
    
    def valida_inputs_aprox(self, inputs):
        funciones = Funciones()
        required_keys = [
            "neto"
        ]
        resultado = funciones.in_dictionary(required_keys, inputs)
        if resultado.get('error') is not None:
            return {
                'error': True, 
                'mensaje': 'Error validar keys in dictionary', 
                'data': resultado,
                'file': __file__, 
                'line': inspect.currentframe().f_lineno
            }
        return {
            "mensaje": "Exito"
        }