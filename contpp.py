import sys
from nomina import Nomina
from prettytable import PrettyTable
from tkinter import Tk, Button, ttk, messagebox, Checkbutton, IntVar

class Contpp:
    ejercicioLabel = ""
    salarioDiarioInicioLabel = ""
    salarioDiarioFinalLabel = ""
    diasPagadosLabel = ""
    diasVacacionesLabel = ""
    stepLabel = ""
    totalCalculadosLabel = ""
    ejercicioEntry = None
    salarioDiarioInicioEntry = None
    salarioDiarioFinalEntry = None
    diasPagadosEntry = None
    diasVacacionesEntry = None
    stepEntry = None
    estrategiaBonosChk = None
    nCalculosEjecutarEntry = None
    netoPercibirEntry = None
    tableLabel = ""

    def init_gui(self):
        root = Tk()
        root.title("contpp python")
        tabControl = ttk.Notebook(root)
        asigna = ttk.Frame(tabControl)
        tabControl.add(asigna, text='Valores')
        tabControl.pack(expand = 1, fill ="both")

        ejercicioLabel = ttk.Label(asigna, text='Ejercicio:')
        ejercicioLabel.grid(row=0, column=0, padx=15)
        self.ejercicioEntry = ttk.Entry(asigna)
        self.ejercicioEntry.grid(row=0, column=1, padx=15)

        salarioDiarioInicioLabel = ttk.Label(asigna, text='Salario diario inicial:')
        salarioDiarioInicioLabel.grid(row=1, column=0, padx=15, pady=5)
        self.salarioDiarioInicioEntry = ttk.Entry(asigna)
        self.salarioDiarioInicioEntry.grid(row=1, column=1, padx=15, pady=5)

        salarioDiarioFinalLabel = ttk.Label(asigna, text='Salario diario final:')
        salarioDiarioFinalLabel.grid(row=2, column=0, padx=15, pady=5)
        self.salarioDiarioFinalEntry = ttk.Entry(asigna)
        self.salarioDiarioFinalEntry.grid(row=2, column=1, padx=15, pady=5)

        diasPagadosLabel = ttk.Label(asigna, text='Número días pagados:')
        diasPagadosLabel.grid(row=3, column=0, padx=15, pady=5)
        self.diasPagadosEntry = ttk.Entry(asigna)
        self.diasPagadosEntry.grid(row=3, column=1, padx=15, pady=5)

        diasVacacionesLabel = ttk.Label(asigna, text='Número días vacaciones:')
        diasVacacionesLabel.grid(row=4, column=0, padx=15, pady=5)
        self.diasVacacionesEntry = ttk.Entry(asigna)
        self.diasVacacionesEntry.grid(row=4, column=1, padx=15, pady=5)

        stepLabel = ttk.Label(asigna, text='Step:')
        stepLabel.grid(row=5, column=0, padx=15, pady=5)
        self.stepEntry = ttk.Entry(asigna)
        self.stepEntry.grid(row=5, column=1, padx=15, pady=5)

        self.estrategiaBonosChk = IntVar()
        checkbox = Checkbutton(asigna, text = "Estrategia bonos",
            variable = self.estrategiaBonosChk,
            onvalue = 1,
            offvalue = 0,
            height = 2,
            width = 15)
        checkbox.grid(row=6, column=0, padx=15, pady=5)

        asignaButton = Button(asigna, text="Guardar", command=self.valida_inputs_asignar_base)
        asignaButton.grid(row=7, column=0, padx=60, pady=10)

        netoPercibirLabel = ttk.Label(asigna, text='Neto:')
        netoPercibirLabel.grid(row=8, column=0, padx=15)
        self.netoPercibirEntry = ttk.Entry(asigna)
        self.netoPercibirEntry.grid(row=8, column=1, padx=15)

        aproximacionButton = Button(asigna, text="Consultar", command=self.valida_inputs_aproximacion_neto)
        aproximacionButton.grid(row=9, column=0, padx=60, pady=10)

        self.tableLabel = ttk.Label(asigna, text='')
        self.tableLabel.grid(row=10, column=0, padx=60, pady=10)
        
        root.mainloop()

    def handle_tab_changed(self, event):
        selection = event.widget.select()
        tab = event.widget.tab(selection, "text")
        return

    def valida_inputs_asignar_base(self):
        ejercicio = self.ejercicioEntry.get()
        try:
            ejercicio = int(ejercicio)
        except ValueError:
            messagebox.showerror(title="Dato invalido", message="El ejercicio debe tener un valor númerico.")
            return
        if len(str(ejercicio)) != 4:
            messagebox.showerror(title="Dato invalido", message="El ejercicio debe ser un ejercicio valido 4 digitos.")
            return
        if ejercicio < 2000:
            messagebox.showerror(title="Dato invalido", message="El ejercicio debe ser del siglo XX.")
            return
        nomina_t = Nomina()
        fecha_inicio_ejercicio = str(ejercicio) + "-02-01"
        resultado = nomina_t.obten_salario_minimo_inicial(fecha_inicio_ejercicio)
        if resultado.get('error') is not None:
            messagebox.showerror(title="Sistema", message="Ocurrió un error al obtener salario mínimo")
            return
        salario_minimo = resultado.get('salario_minimo_monto')
        salario_diario_minimo = self.salarioDiarioInicioEntry.get()
        try:
            salario_diario_minimo = float(salario_diario_minimo)
        except ValueError:
            messagebox.showerror(title="Dato invalido", message="El salario diario inicio debe tener un valor númerico.")
            return
        if salario_diario_minimo == 0:
            salario_diario_minimo = salario_minimo
        salario_diario_maximo = self.salarioDiarioFinalEntry.get()
        try:
            salario_diario_maximo = float(salario_diario_maximo)
        except ValueError:
            messagebox.showerror(title="Dato invalido", message="El salario diario final debe tener un valor númerico.")
            return
        if salario_diario_maximo < salario_minimo:
            messagebox.showerror(title="Dato invalido", message="El salario diario inicio no puede ser menor al mínimo oficial.")
            return
        n_dias_pagados = self.diasPagadosEntry.get()
        try:
            n_dias_pagados = int(n_dias_pagados)
        except ValueError:
            messagebox.showerror(title="Dato invalido", message="Los días pagados debe tener un valor númerico.")
            return
        n_dias_vacaciones = self.diasVacacionesEntry.get()
        try:
            n_dias_vacaciones = int(n_dias_vacaciones)
        except ValueError:
            messagebox.showerror(title="Dato invalido", message="Los días de vacaciones debe tener un valor númerico.")
            return
        step = self.stepEntry.get()
        try:
            step = float(step)
        except ValueError:
            messagebox.showerror(title="Dato invalido", message="El step debe tener un valor númerico.")
            return
        estrategia_bonos = self.estrategiaBonosChk.get()
        resultado = nomina_t.limpia_base_sueldos_salarios()
        if resultado.get('error') is not None:
            messagebox.showerror(title="Sistema", message="Ocurrio un error al limpiar BD.")
            return
        valores = {
            "ejercicio": ejercicio,
            "sd_inicio": salario_diario_minimo,
            "sd_final": salario_diario_maximo,
            "n_dias_pagados": n_dias_pagados,
            "n_dias_vacaciones": n_dias_vacaciones,
            "estrategia_bonos": estrategia_bonos,
            "step": step
        }
        resultado = nomina_t.asigna_valores_iniciales(valores)
        if resultado.get('error') is not None:
            messagebox.showerror(title="Sistema", message="Ocurrio un error al asignar valores iniciales BD.")
            return
        resultado = nomina_t.calcula_valores_sueldos_salarios()
        if resultado.get('error') is not None:
            messagebox.showerror(title="Sistema", message="Ocurrio un error al calcular valores sueldos salarios.")
            return
        messagebox.showinfo(title="Sistema", message="Exito")
        return

    def valida_inputs_aproximacion_neto(self):
        neto = self.netoPercibirEntry.get()
        try:
            neto = float(neto)
        except ValueError:
            messagebox.showerror(title="Dato invalido", message="El neto debe tener un valor númerico.")
            return
        valores = {
            "neto": neto
        }
        nomina_t = Nomina()
        resultado = nomina_t.obten_aproximacion_neto(valores)
        if resultado.get('error') is not None:
            messagebox.showerror(title="Sistema", message="Ocurrió un error al obtener aproximación al neto.")
            return
        table = PrettyTable(['SD', 'SDI', 'NETO'])
        for key in resultado:
            obj = resultado[key]
            sd = obj.get('sd')
            sdi = obj.get('sdi')
            neto = obj.get('neto')
            table.add_row([sd, sdi, neto])
        self.tableLabel.config(text = table)

contpp = Contpp()
contpp.init_gui()